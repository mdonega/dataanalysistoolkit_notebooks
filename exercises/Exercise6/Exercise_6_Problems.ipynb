{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 6: Arbitrary distributions, moving averages, and Monte-Carlo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import datetime\n",
    "import scipy.stats as stats\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Sampling from an arbitrary distribution\n",
    "As seen in exercise 4, you can use uniformly distributed random variables, which are in principle themselves simple to generate, to draw samples from the normal distribution via the Box-Muller transform. A more general approach is to sample according to the inverse of the cumulative distribution function (CDF).\n",
    "\n",
    "A simple example is to generate numbers from the exponential distribution.\n",
    "\n",
    "$$ f(t;\\lambda) = \\lambda e^{-\\lambda t} $$\n",
    "\n",
    "* Write the CDF $F(T,\\lambda)$ and find its inverse ($T=...$)\n",
    "* Write a function to compute this, and compare your result to that from scipy (hint: sometimes called percent-point function or quantile function)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Quantile function\n",
    "def exp_quantile(p, l):\n",
    "    ...\n",
    "\n",
    "p = np.linspace(0, 1, 100)\n",
    "l = 0.2\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Now draw N samples from the uniform distribution $[0,1]$. For each sample, calculate $F^{-1}(u,\\lambda)$\n",
    "* Plot a histogram and compare the distribution of points to the exponential pdf\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 1000\n",
    "l = 0.2\n",
    "x = ...\n",
    "y = ...\n",
    "\n",
    "...\n",
    "\n",
    "print('Actual lambda:', l)\n",
    "print('Estimated lambda: ', ...)\n",
    "\n",
    "...\n",
    "\n",
    "# Check the fit\n",
    "...\n",
    "\n",
    "# Plot histogram, fit, and calculated pdf\n",
    "...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Smoothing data\n",
    "## 2.1 Moving average\n",
    "The moving average, or rolling mean, is a simple technique which can be used to remove short term or periodic (e.g. seasonal) variations in time series data, for example. It can be viewed as a \"smoothing\", and can ease trend spotting, for instance. One has to be careful when interpreting and using the result; for instance, it is generally improper to fit on such data.\n",
    "\n",
    "The simplest moving average can be computed using a \"sliding window\" of length $N$, with all weights equal. For example, for a 3 point moving average, the window would be $\\frac{1}{3}[1,1,1]$.\n",
    "\n",
    "* Write a function to compute the $N$ point moving average of a data series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def moving_average(y, length):\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following line of code loads a dataset (into a ```pandas DataFrame```) containing monthly measurements of variation in the global surface temperature, stretching back as far as 1750. (More data like this can be found on http://berkeleyearth.org)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('Material/Complete_TAVG_complete.txt', skipinitialspace=True, delimiter=' ', comment='%')\n",
    "df['Date'] = df.apply(lambda row: datetime.datetime(\n",
    "                              int(row['Year']), int(row['Month']), 15), axis=1)\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Plot the data. To plot the monthly differences, for example, you can directly write ```df2['MDiff'].plot()```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For example...\n",
    "df.query('Year>1980 & Year<2000').plot(x='Date', y='MDiff')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Apply your moving average filter to the monthly data ```MDiff```. Try (for example) 6 months, 5 years, 10 years. Plot these on top of cuts of the original data to compare."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Electronic response of RC circuit\n",
    "\n",
    "In general, the response of a linearly time invariant system is found to be the convolution of the its impulse response $h(t)$ and the input voltage. Consider a resistor and capacitor connected in series, driven by a time-varying voltage $u(t)$. The impulse response for such a circuit is:\n",
    "\n",
    "$$h_c(t) = \\frac{1}{RC} e^{-t/RC} u(t)$$\n",
    "\n",
    "* Write a function to calculate the impulse response as a function of time, the resistance, and the capacitance, and input. Take care to normalise the integral.\n",
    "\n",
    "* Now consider a noisy sinusoidal input voltage $u_N(t) = u(t) + \\epsilon(t)$, where $u(t)=sin(2\\pi f_1 t) + cos(2\\pi f_2 t)$, and $\\epsilon$ is a vector comprising samples draw from $N~(0,1)$. $f_1$ should be a lower frequency (~factor 10) than ($f_2$), where the cosine represents a faster ripple riding the fundamental tone. Plot the noisy signal and superimpose the clean signal.\n",
    "\n",
    "* Calculate the circuit response for your signal and compare the result to the noisy signal and the clean, original signal\n",
    "\n",
    "* Play with the RC time constant and see the effect on the signal. \n",
    "\n",
    "Note: this first order low pass filter is exactly equivalent to an exponential moving average. The \"memory\" of the output is effectively determined by the time constant.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rc_impulse(t, R, C):\n",
    "    # Impulse response\n",
    "    ...\n",
    "\n",
    "def rc_response(t, u, R, C):\n",
    "    # Cumulative response\n",
    "    ...\n",
    "\n",
    "t = np.linspace(0, 0.1, 5000)\n",
    "dt = t[1]-t[0]\n",
    "R = 5e3\n",
    "C = 100e-9\n",
    "tc = R*C\n",
    "\n",
    "f1 = 200\n",
    "f2 = 0.1 * f1\n",
    "u = ...\n",
    "un = ...\n",
    "\n",
    "print('Cutoff: ', tc)\n",
    "\n",
    "...\n",
    "\n",
    "# Try different cutoffs (remove noise, fast ripple, then whole thing)\n",
    "...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Monte Carlo methods\n",
    "### 3.1. Particle propagation\n",
    "The elementary processes of particle absorption and scattering are random in their nature. Propagation of particles through a slab of material with multiple scattering events may be impossible to calculate analytically, but can easily be simulated with Monte Carlo methods.\n",
    "\n",
    "* Consider a beam of photons propagating through an absorbing medium with absorption coefficient $\\alpha=0.2$ per unit length. What is the probability of a photon being absorbed in a unit length slab of material?\n",
    "\n",
    "* Now take a piece of 1D material made up of 100 slices, each unit length. Starting at x=0, propagate a beam of 1000 photons through the material, slice-by-slice. At each interface, you should \"measure\" each photon to determine whether it has been transmitted or absorbed (hint: uniform distribution, $P(abs)$)\n",
    "\n",
    "* Plot the number of photons which are transmitted at the end of each slice, and compare that to the Beer-Lambert-Bouger law\n",
    "\n",
    "* Plot a histogram of the distance travelled before absorption for each photon (free paths).\n",
    "\n",
    "$I(x) = I_{0}e^{-\\alpha x }$ , where $\\alpha$ is absorption coefficient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N_slices = 100 # Slices of material\n",
    "N_particles = 1000 # Number of particles to simulate\n",
    "alpha = 0.2 # absorption coefficient\n",
    "P_abs = ...\n",
    "\n",
    "# Generate N_slices x N_particles matrix of uniformly distributed random numbers. \n",
    "# Transform it into a matrix of absorption events, where True = absorption, False = no absorption, \n",
    "# mean(Abs_events) = P_abs\n",
    "Abs_events = ...\n",
    "\n",
    "...\n",
    "\n",
    "print('Generated absorption probability (mean) = ', np.mean(Abs_events))\n",
    "print('Fraction of escaped particles = ',N_escaped_final/N_particles)\n",
    "\n",
    "# Plots\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2. Monte-Carlo integration: estimate $\\pi$\n",
    "\n",
    "In a so-called ’hit-and-miss’ approach, or ’simple sampling’, one can estimate the integral\n",
    "of an arbitrary, well-behaved function over some interval by scattering many points over\n",
    "some rectangular area A. The probability of a point landing below the curve is proportional\n",
    "to the function’s integral.\n",
    "A classic problem is to determine the value of π.\n",
    "\n",
    "* Uniformly distribute N points over a unit area. Plot these on top of a unit circle (or quarter circle)\n",
    "* Calculate the proportion that are within the bounds of your shape for some number of samples N (for large N, it would be unwise to plot)\n",
    "* Repeat the exercise for increasing N. For each run, you should compute and store the error $\\epsilon = \\bar{\\pi} - \\pi$\n",
    "* Plot log-log the convergence of your estimate to the actual value (to machine precision) of $\\pi$, i.e. $\\epsilon$ vs the number of points $N$. Compare this to the expected rate of convergence $(1/\\sqrt N)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Anaconda3]",
   "language": "python",
   "name": "Python [Anaconda3]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
