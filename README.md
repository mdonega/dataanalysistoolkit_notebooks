# DataAnalysisToolkit

This repository collects material for the **Introduction to data analysis for physics laboratories**.

Several TAs and students helped with this repository and gave valuable feedback.

Among them I would like to thank in particular:
Pirmin Berger, 
Patrick Knueppel, 
Micheal Reichmann, 
Daniele Ruini, 
Diego Sanz,
Matthew Singleton
